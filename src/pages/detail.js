import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PostsDetail from "../components/PostsDetail"

const IndexPosts= (props) => (
  <Layout title="Detalle POSTS" {...props}>
    <SEO title="Detail de POSTS" />
    <PostsDetail {...props}/>
  </Layout>
)

export default IndexPosts
