import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ListPosts from "../components/ListPosts"

const IndexPage = (props) => (
  <Layout title="Listado de POSTS">
    <SEO title="Listado de POSTS" />
    <ListPosts {...props}/>
  </Layout>
)

export default IndexPage
