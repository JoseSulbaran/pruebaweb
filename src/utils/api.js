export async function handleConexionFetch(ruta, data, type = "") {
  return await new Promise(function (resolve, reject) {
    fetch(ruta, data)
      .then(res => res.json())
      .then(response => resolve(response))
      .catch(error => reject(error))
  })
}